package com.raul.buffer.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Cliente produtor do buffer
 */
public class Produtor extends ClienteBuffer {

    /**
     * Constr�i um novo produtor
     *
     * @param ip IP do servidor de buffer
     * @param porta porta do servidor de buffer
     * @param id Identificador do produtor
     */
    public Produtor(String ip, int porta, int id) {
        super(ip, porta, id);
    }

    /**
     * M�todo respons�vel por enviar um novo n�mero para o buffer via socket e
     * receber o resultado
     */
    private void produzir() {
        int numero = new Random().nextInt();

        try {
            ObjectOutputStream saida = new ObjectOutputStream(this.conexao.getOutputStream());
            saida.flush();

            ObjectInputStream entrada = new ObjectInputStream(this.conexao.getInputStream());

            HashMap<String, Object> mensagem = new HashMap();
            mensagem.put("operacao", "+");
            mensagem.put("quem", this.nome);
            mensagem.put("item", numero);

            saida.writeObject(mensagem);
            long tempoInicial = System.currentTimeMillis();
            saida.flush();
            HashMap<String, Object> mensagemServidor = (HashMap) entrada.readObject();
            long tempoTotal = System.currentTimeMillis() - tempoInicial;

            if (true == ((boolean) mensagemServidor.get("status"))) {
                System.out.println("Colocado o valor " + numero + " no Buffer pelo " + nome + " em " + tempoTotal + " milissegundos");
            } else {
                if ("BUFFER_CHEIO".equals(mensagemServidor.get("codigo"))) {
                    System.out.println(nome + " tentou colocar item no Buffer cheio");
                }
            }
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Produtor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        if (null != conexao) {
            produzir();
            desconectar();
        }
    }
}
