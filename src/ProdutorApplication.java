
import com.raul.buffer.client.Produtor;

/**
 * Aplicação responsável por rodar produtores
 */
public class ProdutorApplication {

    public static void main(String[] args) {
        if (args.length < 3) {
            System.out.println("Parâmetros inválidos");
            return;
        }
        int threads;
        int porta;
        try {
            threads = Integer.parseInt(args[0]);
            porta = Integer.parseInt(args[2]);
        } catch (NumberFormatException ex) {
            System.out.println("Parâmetros inválidos");
            return;
        }
        String ip = args[1];

        for (int i = 0; i < threads; i++) {
            new Thread(new Produtor(ip, porta, i + 1)).start();
        }
    }
}
